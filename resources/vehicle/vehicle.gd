class_name Vehicle

@export var body_mesh : MeshInstance3D
@export var wheel_mesh : MeshInstance3D

enum traction {
	FWD,
	RWD,
	AWD
}


