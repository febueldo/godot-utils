@tool
extends Node3D

const rocks_number : int = 40
var nodes : Array[StaticBody3D]

func pebbles():

	var rock_collision_shape = BoxShape3D.new()
	var rock_instance_mesh = BoxMesh.new()
	var rock_instance_mesh_material = ShaderMaterial.new()
	rock_instance_mesh_material.shader = preload("res://scenes/auxiliary/blueprint.gdshader")
	rock_instance_mesh.material = rock_instance_mesh_material



	for i in range(rocks_number):
		var rock = StaticBody3D.new()

		var rock_collision = CollisionShape3D.new()
		rock_collision.shape = rock_collision_shape

		var rock_instance = MeshInstance3D.new()
		rock_instance.mesh = rock_instance_mesh

		var shape_rot = Vector3(randf() * 2 * PI, randf() * 2 * PI, randf() * 2 * PI)
		var shape_loc = Vector3(randi_range(-16, 16), 0, randi_range(-16, 16))
		var shape_sca = Vector3.ONE * (0.25 + randf() * 0.75) * 0.5

		rock.add_child(rock_collision)
		rock.add_child(rock_instance)

		rock.rotation = shape_rot
		rock.position = shape_loc
		rock.scale = shape_sca

		nodes.append(rock)

	for node in nodes:
		add_child(node)

func _ready():
	pass
