extends VehicleBody3D

@export var max_steering : float = .5;
@export var max_engine : float = 80
@export var max_brake : float = 1

var suspension_stiffness = 110
var suspension_travel = 0.25
var suspension_max_force = 8000

var wheels : Array[VehicleWheel3D]


func _physics_process(delta):
	
	var velocity = transform.basis.z.dot(linear_velocity.normalized()) * linear_velocity.length()

	gravity_scale = 1 + absf(0.5 * velocity)
	steering = move_toward(steering, Input.get_axis("right", "left") * max_steering, delta * 2.5)
	engine_force = Input.get_axis("backwards", "forward") * max_engine
	brake = int(Input.is_action_pressed("brake")) * max_brake

	if Input.is_action_pressed("backwards") and velocity > 0:
		brake = max_brake
	elif Input.is_action_pressed("forward") and velocity < 0:
		brake = max_brake
	
#	for wheel in wheels:
#		wheel.suspension_stiffness = suspension_stiffness + 10 * velocity
	
	
func _ready():
	mass = 100
	for child in get_children():
		if child is VehicleWheel3D:
			wheels.append(child)

	for wheel in wheels:
		wheel.suspension_stiffness = suspension_stiffness
		wheel.suspension_travel = suspension_travel
		wheel.suspension_max_force = suspension_max_force
