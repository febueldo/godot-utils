extends Camera3D

@export_category("Camera")
@export var draw_mesh_instance : bool = true

var dof_initial_distance = 10
var dof_distance_offset = 5

var mesh_instance : MeshInstance3D
const RAY_LENGTH = 1000

func update_dof(dof_distance, dof_offset, dof_amount):
	if attributes.dof_blur_far_enabled:
		attributes.dof_blur_far_distance = dof_distance + dof_offset
	if attributes.dof_blur_near_enabled:
		attributes.dof_blur_near_distance = dof_distance - dof_offset
	attributes.dof_blur_amount = dof_amount

func _ready():
	if draw_mesh_instance:
		mesh_instance = MeshInstance3D.new()
		mesh_instance.mesh = SphereMesh.new()
		add_child(mesh_instance)

	attributes = CameraAttributesPractical.new()
	attributes.dof_blur_far_enabled = true
	attributes.dof_blur_far_distance = dof_initial_distance + dof_distance_offset
	attributes.dof_blur_near_enabled = true
	attributes.dof_blur_near_distance = dof_initial_distance - dof_distance_offset

func _physics_process(_delta):
	var space_state = get_world_3d().direct_space_state
	var mousepos = get_viewport().get_mouse_position()

	var origin = project_ray_origin(mousepos)
	var end = origin + project_ray_normal(mousepos) * RAY_LENGTH
	var query = PhysicsRayQueryParameters3D.create(origin, end)
	query.collide_with_areas = true

	var result = space_state.intersect_ray(query)
	if not result.is_empty():
		var length = (result.position - global_position).length()
		update_dof(length, length * 0.5, 1 / length * 0.1)
		if draw_mesh_instance:
			mesh_instance.global_position = result.position
