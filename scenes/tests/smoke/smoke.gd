extends Node3D

@onready var smoke_origin = $SmokeOrigin
@onready var camera = $View/Camera3D

const RAY_LENGTH = 1000

@export var voxel_world_dimensions = Vector3i(32, 8, 32)

enum VoxelType {
	FREE,
	EFFECTOR,
	SMOKE
}

var voxel_world : Array[VoxelType]
var voxel_world_node : Node3D
var voxels : Array[Voxel]


# vector math

func distancei(from : Vector3i, to : Vector3i) -> float:
	# returns the distance between two given points
	var vect = from - to
	return sqrt(vect.x** 2 + vect.y ** 2 + vect.z ** 2)

func distance(from : Vector3, to : Vector3) -> float:
	# returns the distance between two given points
	var vect = from - to
	return sqrt(vect.x** 2 + vect.y ** 2 + vect.z ** 2)



# navigate Voxel World
#	row		column	page
#	i		j		k
#	x		y		z

func coo_to_idx(coordinates : Vector3i) -> int:
	# returns Voxel Unit index inside Voxel World with given `coordinates`
	return coordinates.z * voxel_world_dimensions.y * voxel_world_dimensions.x + \
		coordinates.y * voxel_world_dimensions.x + \
		coordinates.x

func idx_to_coo(index : int) -> Vector3i:
	# returns Voxel Unit coordinates inside Voxel World with given `index`
	return Vector3i(
		index % voxel_world_dimensions.x,
		(index / voxel_world_dimensions.x) % voxel_world_dimensions.y,
		index / (voxel_world_dimensions.x * voxel_world_dimensions.y)
	)

func coo_to_pos(coordinates : Vector3i) -> Vector3:
	return Vector3(coordinates) -  Vector3(voxel_world_dimensions) * Vector3(0.5, 0, 0.5)

func pos_to_coo(coordinates : Vector3) -> Vector3i:
	return Vector3i(coordinates + Vector3(voxel_world_dimensions) * Vector3(0.5, 0, 0.5))



# populate Voxel World

func create_voxel_world(dimension : Vector3i) -> Array[VoxelType]:
	# creates a Voxel World with given dimension {x * y * z}
	# returns an array of Voxel.FREE elements of the Voxel World

	var array : Array[VoxelType]= []
	for i in range(dimension.x * dimension.y * dimension.z):
		array.append(VoxelType.FREE)
	return array

func clear_voxel_world() -> void:
	for i in range(voxel_world.size()):
		free_voxel_idx(i)

func update_voxel(index : int, voxel : VoxelType) -> void:
	# updates Voxel Unit in Voxel World at `index` with given `voxel`
	voxel_world[index] = voxel

func add_voxel_smoke(_smoke_origin : Vector3, smoke_radius : float) -> void:
	# FIXME
	# updates Voxel World unit to Voxel.SMOKE if voxel is located within `smoke_radius`
	for i in range(voxel_world.size()):
		if voxel_world[i] != VoxelType.EFFECTOR and distance(coo_to_pos(idx_to_coo(i)), _smoke_origin) < smoke_radius:
#			print_debug("updated %s" % i)
			update_voxel(i, VoxelType.SMOKE)

func add_voxel_collision(collision_position : Vector3i):
	# updates Voxel World unit to Voxel.EFFECTOR at given `collision_position`
	update_voxel(coo_to_idx(collision_position), VoxelType.EFFECTOR)

func free_voxel_idx(voxel_index : int):
	update_voxel(voxel_index, VoxelType.FREE)

func free_voxel_pos(voxel_position : Vector3i):
	update_voxel(coo_to_idx(voxel_position), VoxelType.FREE)



# Voxel Unit

func create_voxel(
		voxel_size : float,
		color : Color = Color(randf_range(0,1), randf_range(0,1), randf_range(0,1))
	) -> MeshInstance3D:
	# creates a simple Voxel Unit as a MeshInstance3D
	# returns the MeshInstance3D

	# creates a StandardMaterial3D with randomized color
	var mesh_material = StandardMaterial3D.new()
	mesh_material.albedo_color = color

	# creates a BoxMesh of given `voxel_size` and assigns the StandardMaterial3D
	var mesh = BoxMesh.new()
	mesh.size = Vector3(voxel_size, voxel_size, voxel_size)
	mesh.material = mesh_material

	# creates a MeshInstance3D and assigns the BoxMesh
	var mesh_instance : MeshInstance3D = MeshInstance3D.new()
	mesh_instance.mesh = mesh

	return mesh_instance



# populate Scene

func append_voxel_to_world(voxel : MeshInstance3D, voxel_position : Vector3, parent : Node3D = self) -> void:
	# appends given `voxel` to world (should be used if voxel_world_position is occupied by Voxel.SMOKE)
	voxel.position = voxel_position + Vector3(0.5, 0.5, 0.5)
	parent.add_child(voxel)

func update_voxel_world():
	for i in range(voxel_world.size()):
		if voxel_world[i] == VoxelType.FREE:
			pass
		if voxel_world[i] == VoxelType.EFFECTOR:
			pass
		if voxel_world[i] == VoxelType.SMOKE:
			pass

func create_smoke() -> Node3D:

	var smoke = Node3D.new()
	
	add_voxel_smoke(smoke_origin.position, 6)

	for i in range(voxel_world.size()):
		if voxel_world[i] != VoxelType.EFFECTOR and voxel_world[i] == VoxelType.SMOKE:
			append_voxel_to_world(create_voxel(1), coo_to_pos(idx_to_coo(i)), smoke)

	return smoke

func free_smoke(smoke : Node3D) -> void:
	for child in smoke.get_children():
		child.queue_free()
	smoke.queue_free()

func get_click():
	var space_state = get_world_3d().direct_space_state
	var mousepos = get_viewport().get_mouse_position()

	var origin = camera.project_ray_origin(mousepos)
	var end = origin + camera.project_ray_normal(mousepos) * RAY_LENGTH
	var query = PhysicsRayQueryParameters3D.create(origin, end)
	query.collide_with_areas = true

	return space_state.intersect_ray(query)


func _ready():
	voxel_world = create_voxel_world(voxel_world_dimensions)



func _physics_process(_delta):
	pass

var last_smoke : Node3D

func _input(_event):
	if Input.is_action_just_released("click"):
		clear_voxel_world()
		if last_smoke:
			free_smoke(last_smoke)
			last_smoke = null
		
		var res = get_click()
		if res.is_empty():
			return
		smoke_origin.position = res.position

		last_smoke = create_smoke()
		add_child(last_smoke)
