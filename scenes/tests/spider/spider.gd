extends Node3D


@export var move_speed : float = 2.5
@export var turn_speed : float = 1
@export var ground_offset : float = .5

@onready var leg_fl = $Armature/Targets/FLTarget
@onready var leg_fr = $Armature/Targets/FRTarget
@onready var leg_rl = $Armature/Targets/RLTarget
@onready var leg_rr = $Armature/Targets/RRTarget

@onready var laser = $Armature/Skeleton3D/BotMesh/OmniLight3D/Laser

func _process(delta):

	var plane1 = Plane(leg_rl.global_position, leg_fl.global_position, leg_fr.global_position)
	var plane2 = Plane(leg_fr.global_position, leg_rr.global_position, leg_rl.global_position)

	var avg_normal = ((plane1.normal + plane2.normal) / 2).normalized()

	var target_basis = _basis_from_normal(avg_normal)
	transform.basis = lerp(transform.basis, target_basis, move_speed * delta).orthonormalized()

	var avg = (leg_fl.position + leg_fr.position + leg_rl.position + leg_rr.position) / 4
	var target_pos = avg + transform.basis.y * ground_offset
	var distance = transform.basis.y.dot(target_pos - position)

	position = lerp(position, position + transform.basis.y * distance, move_speed * delta)

	_handle_mocement(delta)

func _input(_event):
	if Input.is_action_just_pressed("click"):
		laser.enabled = !laser.enabled

func _handle_mocement(delta) -> void:
	var dir = Input.get_axis("forward", "backwards")
	translate(Vector3(0,0,-dir) * move_speed * delta)

	var a_dir = Input.get_axis("right", "left")
	rotate_object_local(Vector3.UP, a_dir * turn_speed * delta)

func _basis_from_normal(normal : Vector3) -> Basis:
	var result = Basis()

	result.x = normal.cross(transform.basis.z)
	result.y = normal
	result.z = transform.basis.x.cross(normal)

	result.x *= scale.x
	result.y *= scale.y
	result.z *= scale.z

	result = result.orthonormalized()

	return result

