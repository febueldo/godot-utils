extends Marker3D

@export var step_target : Node3D
@export var step_distance : float = 1.25
@export var animation_speed : float = 5

@export var adja_target : Node3D
@export var oppo_target : Node3D

var is_stepping : bool = false

var step_duration : float = 0.5 / animation_speed

func _process(_delta):
	if !is_stepping and !adja_target.is_stepping and abs(global_position.distance_to(step_target.global_position)) > step_distance:
		step()
		oppo_target.step()

func step():
	var target_pos = step_target.global_position
	var half_way = (global_position + step_target.global_position) / 2
	is_stepping = true

	var tween = get_tree().create_tween()
	tween.tween_property(self, "global_position", half_way + owner.basis.y, step_duration) 
	tween.tween_property(self, "global_position", target_pos, step_duration)
	tween.tween_callback(func(): is_stepping = false)
