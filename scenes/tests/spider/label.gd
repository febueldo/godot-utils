extends Label3D

var str_ch = "0123456789ABCDEF"
@export_range(0.1, 1.5, 0.05) var refresh_time : float = 0.1
@onready var timer = $Timer
var timer_label = Timer.new()

func _ready():
	timer.timeout.connect(_label_refresh)
	timer.start()

	timer_label.wait_time = refresh_time
	timer_label.timeout.connect(_update_label)

	add_child(timer_label)

func _label_refresh():
	timer_label.start()

func _update_label():
	text = str_ch[randi() % str_ch.length()] + str_ch[randi() % str_ch.length()]
