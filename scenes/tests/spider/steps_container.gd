extends Node3D

@export var offset : float = 18

@onready var parent = get_parent_node_3d()
@onready var prev_position = parent.global_position

func _physics_process(_delta):
	var velocity = parent.global_position - prev_position
	global_position = parent.global_position + velocity * offset

	prev_position = parent.global_position
