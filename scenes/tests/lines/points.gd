extends Node2D

@export_subgroup("Line")
@export_range(1, 0xff) var line_quantity : int = 0xf
@export_range(1, 0xff) var line_width : int = 0xf

@export_subgroup("Point")
@export_range(1, 0xff) var point_quantity : int = 0xf
@export_range(1, 0xff) var point_offset : int = 0xff

@export_subgroup("Timer")
@export_range(0.1, 1.5, 0.05) var wait_time : float = 8



var lines : Array[Line2D]

func random_point(offset : int = point_offset) -> Vector2:
	# returns a random Vector2 inside viewport +/- offset
	var view_size : Vector2 = get_viewport().size
	@warning_ignore("narrowing_conversion")
	return Vector2(
					float(randi_range(0 - offset, view_size.x + offset)),
					float(randi_range(0 - offset, view_size.y + offset))
				)

func create_lines() -> Array[Line2D]:
	for i in range(line_quantity):
		var line = Line2D.new()
		line.width = line_width
		line.joint_mode = Line2D.LINE_JOINT_BEVEL
		line.begin_cap_mode = Line2D.LINE_CAP_ROUND
		line.end_cap_mode = Line2D.LINE_CAP_ROUND
		line.sharp_limit = 10
		line.round_precision = 100
		line.antialiased = true
		for j in range(point_quantity):
			line.add_point(random_point())
		lines.append(line)
	return lines

func add_lines_to_scene() -> void:
	for _line in lines:
		add_child(_line)

func update_points(_line : Line2D) -> void:
	for i in range(_line.points.size()):
		_line.points[i] = Vector2(random_point())

func update_lines(_lines : Array[Line2D]) -> void:
	for _line in _lines:
		update_points(_line)

func _ready():
	create_lines()
	add_lines_to_scene()

	var timer = Timer.new()
	timer.wait_time = wait_time
	timer.timeout.connect(update_lines.bind(lines))
	add_child(timer)

	timer.start()
