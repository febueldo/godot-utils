@tool
extends RayCast3D

@export var distance : float = 16
@export var is_shaded : bool = false

@export_color_no_alpha var albedo : Color = Color.ORANGE_RED
@export_color_no_alpha var emission : Color = Color.RED

@onready var ray_mesh = $MeshInstance3D
@onready var ray_particles = $GPUParticles3D
@onready var ray_light = $OmniLight3D

func _update_color(unshaded = false):
	ray_mesh.mesh.material.albedo_color = albedo
	ray_particles.draw_pass_1.material.albedo_color = albedo
	ray_light.light_color = emission

	if !unshaded:
		ray_mesh.mesh.material.emission = emission
		ray_particles.draw_pass_1.material.emission = emission
	else:
		ray_mesh.mesh.material.shading_mode = BaseMaterial3D.SHADING_MODE_UNSHADED
		ray_particles.draw_pass_1.material.shading_mode = BaseMaterial3D.SHADING_MODE_UNSHADED

func _ready():
	_update_color(!is_shaded)

	ray_mesh.mesh.height = 0
	ray_mesh.position.y = 0

	ray_particles.position.y = 0
	ray_particles.emitting = false

	ray_light.position.y = 0
	ray_light.light_energy = 0

	target_position.y = - distance

func _process(_delta):
	var hit_point : Vector3
	force_raycast_update()

	if enabled and is_colliding():
		hit_point = to_local(get_collision_point())

		ray_mesh.mesh.height = hit_point.y
		ray_mesh.position.y = hit_point.y / 2

		ray_particles.position.y = hit_point.y
		ray_particles.emitting = true

#		ray_light.position.y = hit_point.y
#		ray_light.light_energy = 1

	else:
		ray_mesh.mesh.height = 0
		ray_mesh.position.y = 0

		ray_particles.position.y = 0
		ray_particles.emitting = false

#		ray_light.position.y = 0
#		ray_light.light_energy = 0
