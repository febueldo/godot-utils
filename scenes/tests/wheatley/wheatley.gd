extends Node3D

@export var mesh : MeshInstance3D
@export var camera : Camera3D
@export var marker : Marker3D

var mouse_pos : Vector2i = Vector2i.ONE
var view_size : Vector2i = Vector2i.ONE
var mouse_pos_norm : Vector2 = Vector2.ONE
var marker_new_pos : Vector3 = Vector3.ZERO

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	mouse_pos = get_viewport().get_mouse_position()
	view_size = get_viewport().size
	mouse_pos_norm = Vector2(mouse_pos) / Vector2(view_size) - Vector2(0.5, 0.5)

	marker_new_pos = Vector3(mouse_pos_norm.x, - mouse_pos_norm.y, marker.global_position.z)

	marker.global_position = lerp(
		marker.global_position,
		marker_new_pos,
		delta * 0x2
	)

	mesh.look_at(marker.global_position)
