extends Node3D

@onready var camera = $Camera3D
@onready var color_rect = $CanvasLayer/ColorRect

var zoom : bool = false
var fov : Array[float] = [75, 35]

func toggle_zoom():
	zoom = not zoom
	if zoom:
		camera.fov = fov[1]
		color_rect.visible = true
	else:
		camera.fov = fov[0]
		color_rect.visible = false
	

func _ready():
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	zoom = false
	camera.fov = fov[0]
	color_rect.visible = false

func _input(event):
	if event is InputEventMouseButton:
		if event.pressed and event.button_index == 1:
			print_debug("primary fire")
		if event.pressed and event.button_index == 2:
			toggle_zoom()
