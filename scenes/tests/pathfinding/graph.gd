@tool
extends Node

@export var dims : Vector2i = Vector2i(20, 10)

var all_nodes = []

func grid():
	for x in range(dims.x):
		for y in range(dims.y):
			all_nodes.append([x, y])

func neighbors(node):
	var dirs = [[1, 0], [0, 1], [-1, 0], [0, -1]]
	var result = []
	for dir in dirs:
		var neighbor = [node[0] + dir[0], node[1] + dir[1]]
		if 0 <= neighbor[0] < dims.x and 0 <= neighbor[1] < dims.y:
			result.append([node[0] + dir[0], node[1] + dir[1]])
	return result
