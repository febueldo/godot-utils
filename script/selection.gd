@tool
extends Control

@export_category("Menu")

@export var scenes : Array[PackedScene]
@export var settings : PackedScene

func _on_button_press(scene : PackedScene):
	get_tree().change_scene_to_packed(scene)

func _on_exit_press():
	get_tree().quit()

func _add_menu():
	var vbox = VBoxContainer.new()

	for scene in scenes:
		var button = Button.new()

		# snake_case.extension
		var regex = RegEx.new()
		regex.compile("[^/]+(?=\\.\\w+$)")
		button.text = " ".join(regex.search(scene.resource_path).get_string().split("_")).capitalize()

		button.connect("button_down", _on_button_press.bind(scene))
		vbox.add_child(button)

	vbox.add_child(HSeparator.new())

	var settings_button = Button.new()
	settings_button.text = "Settings"
	settings_button.connect("button_down", _on_button_press.bind(settings))

	vbox.add_child(settings_button)

	var exit_button = Button.new()
	exit_button.text = "Quit"
	exit_button.connect("button_down", _on_exit_press)

	vbox.add_child(exit_button)

	add_child(vbox)
	for el in vbox.get_children():
		if el is Button:
			el.grab_focus()
			break

	vbox.set_offsets_preset(Control.PRESET_CENTER)
	vbox.set_anchors_preset(Control.PRESET_CENTER)




func _ready():
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE

	_add_menu()
