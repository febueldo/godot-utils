extends Node

@onready var scene = ProjectSettings.get_setting("application/run/main_scene")

func render_pass():
	RenderingServer.viewport_set_debug_draw(
		get_viewport().get_viewport_rid(),
#		RenderingServer.VIEWPORT_DEBUG_DRAW_OVERDRAW
		RenderingServer.VIEWPORT_DEBUG_DRAW_DISABLED
	)

func _input(_event):
	if Input.is_action_just_pressed("ui_cancel"):
		if get_tree().get_current_scene().scene_file_path == scene:
			get_tree().quit()
		else:
			get_tree().change_scene_to_file(scene)

func _ready():
	Engine.set_max_fps(60)
	render_pass()
