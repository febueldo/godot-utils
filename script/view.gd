extends Marker3D

@export var reposition : bool = false

var camera_position : Vector3
var camera_rotation : Vector3

@onready var camera = $Camera3D

func _ready():
#	camera.look_at(global_position)
	camera_rotation = rotation_degrees # Initial rotation

func _process(delta):
	# Set position and rotation to targets
	position = position.lerp(camera_position, delta * 8)
	rotation_degrees = rotation_degrees.lerp(
		camera_rotation,
		delta * 6
	)

	handle_input(delta)

# Handle input
func handle_input(_delta):
	# Movement
	if reposition:
		var input := Vector3.ZERO

		input.x = Input.get_axis("left", "right")
		input.z = Input.get_axis("forward", "backwards")

		input = input.rotated(Vector3.UP, rotation.y).normalized()

		camera_position += input / 4

		# Back to center
		if Input.is_action_pressed("camera_reset"):
			camera_position = Vector3()

func _input(event):
	# Rotate camera using mouse (hold 'middle' mouse button)
	if event is InputEventMouseMotion:
		if Input.is_action_pressed("camera_rotate"):
			camera_rotation += Vector3(- event.relative.y / 10, - event.relative.x / 10, 0)
