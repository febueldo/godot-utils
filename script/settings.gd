@tool
extends Control

enum type_button {
	CHECK,
	OPTION
}

enum display_mode {
	WINDOWED,
	FULLSCREEN,
	FULLSCREEN_WINDOWED
}

#var settings = [
#	{
#		"name" : "Vsync mode",
#		"type" : type_button.OPTION,
#		"options" : [
#			{
#				"name" : "Disabled",
#				"value": DisplayServer.VSYNC_DISABLED,
#				"callback" : func(): DisplayServer.window_set_vsync_mode(DisplayServer.VSYNC_DISABLED)
#			},
#			{
#				"name" : "Enabled",
#				"value": DisplayServer.VSYNC_ENABLED,
#				"callback" : func(): DisplayServer.window_set_vsync_mode(DisplayServer.VSYNC_ENABLED)
#			},
#			{
#				"name" : "Adaptive",
#				"value": DisplayServer.VSYNC_ADAPTIVE,
#				"callback" : func(): DisplayServer.window_set_vsync_mode(DisplayServer.VSYNC_ADAPTIVE)
#			}
#		]
#	}
#]




func _on_back_pressed():
	get_tree().change_scene_to_file("res://scenes/selection.tscn")

func _add_settings():
	var vbox = VBoxContainer.new()

#	for setting in settings:
#		var hbox = HBoxContainer.new()
#		var label = Label.new()
#		label.text = setting["name"]
#		hbox.add_child(label)
#		if setting["type"] == type_button.OPTION:
#			var option_button = OptionButton.new()
#			for option in setting["options"]:
#				option_button.add_item(option["name"], option["value"])
#			hbox.add_child(option_button)
#		vbox.add_child(hbox)

#	var hbox = HBoxContainer.new()
#	var label = Label.new()
#	label.text = "Vsync mode"
#	hbox.add_child(label)
#	var options = OptionButton.new()
#	vbox.add_child(hbox)

	vbox.add_child(HSeparator.new())
	# Back button
	var back_button = Button.new()
	back_button.text = "Back"
	back_button.connect("button_down", _on_back_pressed)

	vbox.add_child(back_button)

	add_child(vbox)
	for el in vbox.get_children():
		if el is Button:
			el.grab_focus()
			break
	vbox.set_offsets_preset(Control.PRESET_CENTER)
	vbox.set_anchors_preset(Control.PRESET_CENTER)

func _add_info():
	var margin = MarginContainer.new()
	margin.add_theme_constant_override("margin_left", 16)
	margin.add_theme_constant_override("margin_top", 16)
	margin.add_theme_constant_override("margin_right", 16)
	margin.add_theme_constant_override("margin_bottom", 16)
	var hbox = HBoxContainer.new()

	var txt = Label.new()
	txt.text = "Source code available on "
	var url = LinkButton.new()
	url.text = "GitLab"
	url.uri = "https://gitlab.com/febueldo/godot-utils"
	url.tooltip_text = "https://gitlab.com/febueldo/godot-utils"

	hbox.add_child(txt)
	hbox.add_child(url)
	margin.add_child(hbox)
	add_child(margin)

	margin.set_offsets_preset(Control.PRESET_BOTTOM_RIGHT)
	margin.set_anchors_preset(Control.PRESET_BOTTOM_RIGHT)

func _ready():
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE

	_add_settings()
	_add_info()
