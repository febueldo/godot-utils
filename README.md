# Godot Utils

A collection of tests made in Godot Engine.

## Install

Clone or download the repo then add as a project in **Godot 4**.
If you want to export the project from cli just follow the [godot official docs](https://docs.godotengine.org/en/stable/tutorials/editor/command_line_tutorial.html#exporting).
